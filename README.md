# ATouch

## 功能及研发进度
| 功能 | 进度 |
|:-----:|:-----:|
| 键鼠USB连接 | 已完成 |
| ADB连接 | 已完成 |
| WIFI连接 | 已完成 |
| WIFI固件升级 | 已完成 |
| 手机OTG串口连接 | 已完成 |
| 扩展坞投屏串口连接 | 已完成 |
| 体感模拟 | 正在研发 |
| 手柄连接 | 计划研发 |  

## ATouch安卓端源码

![效果图](https://images.gitee.com/uploads/images/2020/0408/110002_b982beff_683968.png "atouch2.png")

![效果图](https://images.gitee.com/uploads/images/2020/0423/150108_a7f80fd5_683968.png "18245@1587624561@2.png")

![gif](https://images.gitee.com/uploads/images/2020/0423/150126_eb0fbd7b_683968.gif "bili_v_d_1587624907338.gif")

![gif](https://images.gitee.com/uploads/images/2020/0423/150325_ff6f7a4a_683968.gif "bili_v_d_1587625362445.gif")

* 介绍博客
[https://www.cnblogs.com/guanglun/p/10927196.html](https://www.cnblogs.com/guanglun/p/10927196.html)  

* ATouch安卓APP源码
[https://gitee.com/guanglunking/ATouch](https://gitee.com/guanglunking/ATouch)  
【开发环境：AndroidStudio】

* ATouch板子源码
[https://gitee.com/guanglunking/ESP32_CH374U](https://gitee.com/guanglunking/ESP32_CH374U)  
【开发环境：Linux SDK:ESP-DIF3.2】

* ATouch安卓后台程序源码
[https://gitee.com/guanglunking/ATouchService](https://gitee.com/guanglunking/ATouchService)   
【开发环境：android-ndk-r13b】

* APP下载地址
[https://gitee.com/guanglunking/ATouch/blob/master/app/release/ATouchV1.3.apk](https://gitee.com/guanglunking/ATouch/blob/master/app/release/ATouchV1.3.apk)

* 淘宝店铺
[https://item.taobao.com/item.htm?id=595635571591](https://item.taobao.com/item.htm?id=595635571591)  

* 演示视频
[https://www.bilibili.com/video/av53687214](https://www.bilibili.com/video/av53687214)  

![设置界面](https://images.gitee.com/uploads/images/2020/0408/110030_b23d7f55_683968.png "atouch3.png")

![设置界面](https://images.gitee.com/uploads/images/2020/0423/150406_ed5d95ff_683968.png "Screenshot_2020-04-23-14-51-37-628_Atouch V1.3.png")




